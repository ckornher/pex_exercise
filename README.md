# The Pre-Employment Exercise

## This is a Prototype

This is a prototype, not production code. This app is complex enough, in my opinion, to warrant creating a prototype, learning from that experience, and identifying the features that should be incorporated into a production version. A real production app would be written for this well-defined final feature set and to fit-in with your logging, testing and deployment conventions and systems. The big question for this implementation is: ”are the benefits worth the cost to output the results in input order”?.

I have had good success on projects like this with this strategy of making a prototype, analyzing and characterizing it, and then re-writing it for production, incorporating the lessons learned, instead of trying to get it all right the first time, and expending a lot of effort on writing test code and documenting a first, sub-optimal version that should probably be rewritten the day it is released. Much of the code from the final prototype would hopefully be refactored into the production app, however.

I have tested the app manually. This prototype was not designed for testability. Unit testing code like this would probably best be done on a production app with dependency injection, interfaces for the components, etc. 

## Focus and Features
I have disabled the heavily algorithmic features of this app that really should be tested thoroughly:

1. Support for images that will not fit in memory by reading loading sub-regions of the image.
2. Support for catching-up when the application has terminated early

The design is heavily influenced by requirements that may or may not be important in the real world, but would certainly be “nice-to-haves”. Since the application reads a billion URLs per run, I decided that:

1. The app should be restartable without loosing a large fraction of the output. Crashes and intentional or unintentional stoppages should not cost an entire run.
2. Hitting URLs repeatedly should be avoided so that the user of this app is not at risk of being blocked and hopefully should not stand out to the URLs' hosts. Once a URL is read, it should not be read again until required. Test URLs, of course need to be read multiple times.
3. Largely because of the first two requirements, URLs are output in the same order as the input. This simplifies the task of rerunning input files automatically without reprocessing URLs that were already processed in previous runs. This should also simplify troubleshooting and post-processing the output to, for example, update the input list for future runs.

There was no requirement for an output file for failures, and I did not create one. The app just logs failures, but a separate failure file is something that probably should be added for production code.

### The Config File

The Config class contains comments for various the settings that configure the application. There are settings for just about everything that can affect memory use and performance. The setting are not optimized for my laptop. In the real world, these settings would be optimized to provide the most processing / cost for available cloud instances. I have manually tested these settings to ensure that they work within reasonable ranges.

The JSON config file could be easily generated by a script that could run potentially hundreds of variations to find the best configuration(s) to optimize performance / cost. 

### Priorities

My Java was a bit rusty, and the concurrency patterns that I used may not be the most modern, but they should be relatively efficient. There are numerous synchronized methods, but I do not believe that the code has any potential deadlocks. All locking follows a well-defined order: i.e. everything calls synchronized methods on the “coordinator” and the coordinator does not make any synchronous calls to anything that calls it. Much inter-thread communication is through blocking queues.

I took your words “focus on speed and resources “ to heart. The pixel scan calls are not optimized in any way. In fact, the results have not been tested for validity.

As I mentioned, the catch-up feature has not been thoroughly tested, so it is disabled in this version. The Java APIs are rich and have evolved over the years and it is not completely clear which calls load the whole image and which do not. I think that I got it right, but I would not want to release an app like this without extensive profiling in the deployment environment to make sure.

It would take a bit more time than I have to thoroughly understand the performance implications of the various image calls and options. Hopefully the code in its current state clearly conveys my intent: to use sub-areas of the image if the entire image will not fit into the memory limits configured for an ImageProcessor instance. Again, this feature is there, but not tested or enabled.

## Performance

In my testing the app spends almost all of its time waiting for a few files. It can be sped-up by increasing the cache size in the config file or reducing the network timeout (not implemented).

No attempt is made to clean-up the cache in the temp-file directory. Cached files are overwritten if they already exist in the cache directory, however, so data should not be corrupted.

## Obvious Improvements
There is one obvious potential code improvement.

This app's ability to use resources efficiently is largely dependent upon its ability to keep as many `ImageInProgress` recods in memory as it can, to be able to wait for time consuming network delays. The current `ImageInProgress` class could be refactored into a more efficient form that would only store the URL (or a reference to the input line number), and the three numbers required for output once the image has been processed. The file path and other information could be dereferenced as soon as the image is processed. This became obvious well into the project and I would spend some time on optimizing this in-prgress list in a future version, perhaps even exploring the costs of saving this information to disk, thereby eliminating the possibility of stalling while waiting for fetches to complete or fail. Some of these potential improvements depend upon there being sufficient (and affordable) disk space and file I/O to make them feasible.

This in-progress list could be eliminated altogether if out-of-order output is acceptable, and that is a question that would need to be answered before writing a production version. Eliminating this requirement would significantly simplify the app, at the cost of robustness and potentially the value of the output. 

Changes to the output file format, to include the input line number, for example, might reduce the downside of this option. An even richer output file format that incorporates the input line and potentially failures and failure reasons would be even more useful. Incorporating failed input line line numbers would allow the app to "catch-up" to the position in the input file, but it would be a much more expensive operation than the current version's catch-up.

This "complete" output file could be post-processed into the specified format and/or readers could be created that ignored the extra fields.

### Related experience
At EBay, I wrote a python/SQLite app that would regularly process hundreds of millions of entries in a few hours. Computers are much faster now, of course. Using a local or remote database or key-value store might be an option for apps like these.

## Thank You

Thanks for this opportunity and I hope that this is good enough to get to the next step!

- Chris
  

