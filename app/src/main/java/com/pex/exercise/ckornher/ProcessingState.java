package com.pex.exercise.ckornher;

public enum ProcessingState {
	INITIALIZED,
	FETCHING,
	NOT_FETCHED,
	FETCHED,
	PROCESSING,
	FAILED_TO_PROCESS,
	COMPLETE;

	public boolean isEndState() {
		switch (this) {
			case NOT_FETCHED:
			case FAILED_TO_PROCESS:
			case COMPLETE:
				return true;

			default:
				return false;
		}
	}

	public boolean hasFileInCache() {
		switch (this) {
			case FETCHED:
			case PROCESSING:
			case FAILED_TO_PROCESS:
			case COMPLETE:
				return true;

			default:
				return false;
		}
	}
}
