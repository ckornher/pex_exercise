package com.pex.exercise.ckornher;


import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/// Keeps output files up-to-date and synchronized as required. The status file will only reflect files with
/// results that have been flushed to the output files.
class ProcessingRecorder implements Runnable {
    private final TopColorApp app;
    private final ProgressFile progressFile;
    final Path outputFilepath;

    private long latestSavedInputLineNumber = -1;
    private long highestInputLineNumber = -1;           // Will be set with the first message processed

    private final int maximumLinesBetweenStatusUpdates;


    private CSVWriter outputWriter;
    private long outputLineCount;

    private boolean stopped;

    private final int maxMillisBetweenFlushes = 10 * 1000;    // Force flush every ten seconds. Could be a config param.

    private Date lastFlushedAt;


    /// A list of lists of images processed, in the order that they appear in the source file
    private final BlockingQueue<List<ImageInProgress>> processedImageLists;

    public ProcessingRecorder(TopColorApp app,
                              ProgressFile progressFile,
                              long startingOutputLineCount,
                              BlockingQueue<List<ImageInProgress>> processedImageLists) throws IOException
    {
        this.app = app;
        this.progressFile = progressFile;
        this.outputLineCount = startingOutputLineCount;
        this.processedImageLists = processedImageLists;
        this.maximumLinesBetweenStatusUpdates = app.config.getMaxLinesBetweenStatusUpdates();

        // Output file
        outputFilepath = Paths.get(app.config.getOutputFile());
        Files.createDirectories(outputFilepath.getParent());
        if(app.reset || !app.supportCatchUp) {
            outputFilepath.toFile().delete();
        }

        /// URLS that the catch-up algorithm found in the output file. Don't output them again.
        outputWriter = new CSVWriter(new FileWriter(app.config.getOutputFile(),true));
    }

    @Override
    public void run() {
        this.lastFlushedAt = new Date();

        boolean finalImageProcessed = false;

        while(true) {
            synchronized(this) {
                if(stopped) { return;}
                try {
                    List<ImageInProgress> imageRecords = processedImageLists.take();
                    for (ImageInProgress imageRecord : imageRecords) {
                        recordImageOutputIfRequired(imageRecord);
                        if(imageRecord.isFinalImage) {
                            finalImageProcessed = true;
                        }
                    }

                    // Cleanup after the progress has been recorded (and potentially flushed)
                    deleteCachedFiles(imageRecords);
                }
                catch (InterruptedException e){
                    // This is a daemon thread, so ignore it
                }

                // All files
                if( finalImageProcessed ) {
                    stop(true);
                    app.completed();
                    return;
                }
            }
        }
    }

    public synchronized void stop(boolean safeToFlushOutput) {
        if(stopped) { return; }
        stopped = true;

        try {
            outputWriter.close();
            if(safeToFlushOutput) {
                // Paranoia: Don't update progress to the latest if something catastrophic happened
                progressFile.write(highestInputLineNumber, outputLineCount);
            }
        } catch (Exception e) {
            // Nothing to do
        }
    }

    /**
     * Write output for files that were successfully completed. Log failures to the console (Log Level `FINER`)
     * @implNote : The status file is updated when  the output file is periodically flushed.
     * @implNote : Therefore the output file may contain entries for more urls than is indicated in the progress file
     */
    private void recordImageOutputIfRequired(ImageInProgress imageRecord) {
        assert (imageRecord.sourceFileLine > highestInputLineNumber);

        if (imageRecord.processingState == ProcessingState.COMPLETE) {
            outputWriter.writeNext(new String[]{
                    imageRecord.url.toString(),
                    imageRecord.firstColor,
                    imageRecord.secondColor,
                    imageRecord.thirdColor});
            outputLineCount++;
        } else {
            Logger.getGlobal().log(Level.FINER, "Image could not be processed at input line: "
                    + imageRecord.sourceFileLine + " : " + imageRecord.url);
        }

        highestInputLineNumber = imageRecord.sourceFileLine;
        try {
            Date now = new Date();      // The Date() check could be performed less frequently for higher performance or eliminated altogether
            if ((highestInputLineNumber - latestSavedInputLineNumber) > maximumLinesBetweenStatusUpdates
                    || imageRecord.isFinalImage
                    || (now.getTime() - lastFlushedAt.getTime()) > maxMillisBetweenFlushes) {
                // Update the status file in sync with the output file
                outputWriter.flush();
                progressFile.write(highestInputLineNumber, outputLineCount);
                latestSavedInputLineNumber = highestInputLineNumber;
                lastFlushedAt = now;
            }
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Could not update progress. Quitting on input line: " + imageRecord.sourceFileLine);
            app.stopWithException(e, false);
        }
    }

    private void deleteCachedFiles(List<ImageInProgress> imageRecords) {
        for (ImageInProgress imageRecord : imageRecords) {
            if( imageRecord.processingState.hasFileInCache()) {
                if( !imageRecord.filePath.toFile().delete()) {
                    Logger.getGlobal().log(Level.SEVERE, "Could not delete cache file " + imageRecord.filePath.toString());
                }
            }
        }
    }
}
