package com.pex.exercise.ckornher;

import com.opencsv.CSVReader;
import com.pex.exercise.ckornher.configuration.Config;
import com.pex.exercise.ckornher.configuration.Converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.System.exit;



class TopColorApp {
	final Boolean supportCatchUp = false;

	class CatchUpInfo {
		final Set<String> urlsToIgnore;
		final long InputLineProcessedCount;
		final long outputFileLineCount;

		CatchUpInfo(Set<String> urlsToIgnore, long InputLineProcessedCount, long outputFileLineCount) {
			this.urlsToIgnore = urlsToIgnore;
			this.InputLineProcessedCount = InputLineProcessedCount;
			this.outputFileLineCount = outputFileLineCount;
		}
	}

	private final File inputFile;
	private final String configFilePath;
	final Boolean reset;
	private boolean stoppedRunning = false;

	Path imageCacheDir;

//	private ImageProcessor[] imageProcessors;

	Config config;
	private ProgressFile progressFile;

	private Coordinator coordinator;
	private ImageFileCollector fileCollector;
	private ProcessingRecorder processingRecorder;
//	protected ArrayList<ImageProcessor> imageProcessors

	TopColorApp(String inputFilePath, String configFilePath, boolean reset) {
		this.inputFile = new File(inputFilePath);
		this.configFilePath = configFilePath;
		this.reset = reset;
	}

	@SuppressWarnings("ConstantConditions")
    synchronized void start() {
		setupLogging();

		try {
			String configJSON = new String ( Files.readAllBytes( Paths.get(configFilePath) ) );
			config = Converter.fromJsonString(configJSON);

			Path tempPath = Files.createTempDirectory("TopColorApp");
			imageCacheDir = tempPath.resolve("imageFileCache");
			Files.createDirectories(imageCacheDir);

			progressFile = new ProgressFile(config.getProgressFile());
			coordinator = new Coordinator(this);

			Logger.getGlobal().log(Level.INFO, "Config: " + config.getConfigDescription() + "\n");

			CatchUpInfo catchUpInfo = catchUp();

			fileCollector = new ImageFileCollector(
					this,
					coordinator,
					inputFile,
					catchUpInfo.InputLineProcessedCount,
					catchUpInfo.urlsToIgnore);

			processingRecorder = new ProcessingRecorder(this,
					progressFile,
					catchUpInfo.outputFileLineCount,
					coordinator.processedImageLists);

			// Create the processors in daemon threads that do not need to explicitly stopped
			for(int i=0; i<config.getImageAnalyzerCount(); i++) {
				ImageProcessor processor = new ImageProcessor(coordinator, config);

				Thread t = new Thread(processor);
				t.setDaemon(true);
				t.start();
			}

			// Start the output writer that waits on the coordinator's `processedImageLists` queue
			Thread t = new Thread(processingRecorder);
			t.setDaemon(true);
			t.start();

			// Start reading files into the cache. The pipeline will process them from there.
			fileCollector.start();
		}
		catch( Exception e) {
			Logger.getGlobal().log(Level.SEVERE, "Could not start", e);
			exit(-1);
		}

		Logger.getGlobal().log(Level.INFO, "Started");
	}

	private void setupLogging() {
		Logger logger = Logger.getGlobal();

		// LOG this level to the log
		logger.setLevel(Level.FINEST);

		ConsoleHandler handler = new ConsoleHandler();
		// PUBLISH this level
		handler.setLevel(Level.FINEST);
		logger.addHandler(handler);

		System.out.println("Logging level is: " + logger.getLevel());
	}

	private CatchUpInfo catchUp() {
		if(reset || !supportCatchUp) {
			progressFile.delete();
		}

		if(reset || !supportCatchUp || ! progressFile.exists()) {
			return new CatchUpInfo(null, 0, 0);
		}

		// Catching-up from a previous run
		// There is probably a better way to do this, but it should be very infrequent
		try( CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader(config.getOutputFile())))) {
			progressFile.read();

			for( int i=0; i < progressFile.associatedOutputLineCount; i++) {
				csvReader.readNext();
			}

			// This list should be no longer than `maximumLinesBetweenStatusUpdates` -- a reasonable number
			List<String[]> unSyncedLines = csvReader.readAll();

			Set<String> urlsToIgnore = new TreeSet<>();
			for(String[] line: unSyncedLines) {
				urlsToIgnore.add(line[0]);
			}

			return new CatchUpInfo(urlsToIgnore,
					progressFile.latestProcessedInputLineNumber,
					progressFile.associatedOutputLineCount + unSyncedLines.size());
		}
		catch( Exception e) {
			Logger.getGlobal().log(Level.SEVERE, "Could not process progress file, please delete the progress file and restart: "
					+ config.getProgressFile().toString() +  " " + e.getLocalizedMessage());
			exit(-1);
		}

		return null;
	}

	synchronized void completed() {
		if( !stoppedRunning ) {
			fileCollector.stop();
			coordinator.stop();
			processingRecorder.stop(true);		// Will not be necessary if triggered by end of input.

			Logger.getGlobal().log(Level.INFO, "Processing Complete. Output in: " + processingRecorder.outputFilepath.toAbsolutePath().toString());
			stoppedRunning = true;

			System.exit(0);
		}
	}

	synchronized void stopWithException(Exception e, boolean safeToFlushOutput) {
		if (!stoppedRunning) {
			fileCollector.stop();
			coordinator.stop();
			processingRecorder.stop(safeToFlushOutput);

			String flushString = safeToFlushOutput ? "Final output flushed." : "Final output was not flushed.";

			Logger.getGlobal().log(Level.INFO, "Processing Stopped with exception. " + flushString, e);
			stoppedRunning = true;

			System.exit(-1);
		}
	}
}