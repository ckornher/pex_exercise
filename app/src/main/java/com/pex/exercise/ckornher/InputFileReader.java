package com.pex.exercise.ckornher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

class InputFileReader {

    private BufferedReader reader;
	private long currentLineNumber;
    private ImageFileCollector collector;

    public InputFileReader(File inputFile, long fromLine, ImageFileCollector collector) throws IOException {
        reader = new BufferedReader(new FileReader(inputFile));
        this.collector = collector;

        // Catch-up
        // There are probably faster ways to do this, but since this is only used for restarts/crash recovery, it will do
        for(int i=0; i<fromLine; i++) {
            String line = reader.readLine();
            if(line == null) {
                throw new IOException("File Too Short");
            }
        }

        currentLineNumber = fromLine;       // Keep things zero-based and simplify incrementing logic
    }

    public synchronized ImageInProgress nextImageToProcess(double estimatedFileSize) throws IOException {
        currentLineNumber++;
        String urlLine =  reader.readLine();
        if( urlLine == null ) {
            return null;
        }

        URL fileURL = new URL(urlLine.trim());
        return new ImageInProgress(fileURL, currentLineNumber, collector, estimatedFileSize);
    }
}
