package com.pex.exercise.ckornher.configuration;


// Config.java

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.File;

public class Config {
    private String configDescription;

    private String outputFile;
    private String workingDirectory;
    private int maxSimultaneousFileFetches;
    private long initialAverageImageSize;
    private long cacheSize;

    private int maxInProgressImages;

    private int imageAnalyzerCount;
    private long maxImageMemory;
    private long maxTotalImageSize;
    private int maxColorsPerImage;

    private int maxLinesBetweenStatusUpdates;

    // Files

    /**
     * A description of these settings, e.g. the instance type(s) that this file should be used for.
     */
    @JsonProperty("configDescription")
    public String getConfigDescription() { return configDescription; }
    @JsonProperty("configDescription")
    public void setConfigDescription(String value) { this.configDescription = value; }

    /**
     * The path the the output file. This file will be appended to if it exists and a progress file `<workingDirectory>/progress.csv` is present.
     */
    @JsonProperty("outputFile")
    public String getOutputFile() { return outputFile; }
    @JsonProperty("outputFile")
    public void setOutputFile(String value) { this.outputFile = value; }

    /**
     *The root of all temporary and cache files
     */
    @JsonProperty("workingDirectory")
    public String getWorkingDirectory() { return workingDirectory; }
    @JsonProperty("workingDirectory")
    public void setWorkingDirectory(String value) { this.workingDirectory = value; }

    /**
     * The maximum image memory to use in each image processor
     */
    @JsonProperty("maxImageMemory")
    public long getMaxImageMemory() { return maxImageMemory; }
    @JsonProperty("maxImageMemory")
    public void setWMaxImageMemory(long value) { this.maxImageMemory = value; }

    /**
     * The maximum size image to process. This is the image width * height
     */
    @JsonProperty("maxTotalImageSize")
    public long getMaxTotalImageSize() { return maxTotalImageSize; }
    @JsonProperty("maxTotalImageSize")
    public void setMaxTotalImageSize(long value) { this.maxTotalImageSize = value; }

    /**
     * The maximum number of colors to track in each image
     */
    @JsonProperty("maxColorsPerImage")
    public int getMaxColorsPerImage() { return maxColorsPerImage; }
    @JsonProperty("maxColorsPerImage")
    public void setMaxColorsPerImage(int value) { this.maxColorsPerImage = value; }

    /**
     * The progress file allows the system to pick-up more where it left off after an intentional or unintentional stop
     */
    public File getProgressFile() { return new File(workingDirectory, "progress.csv"); }


    // Caching

    /**
     * The maximum number of open channels reading files into the disk cache
     */
    @JsonProperty("maxSimultaneousFileFetches")
    public int getMaxSimultaneousFileFetches() { return maxSimultaneousFileFetches; }
    @JsonProperty("maxSimultaneousFileFetches")
    public void setMaxSimultaneousFileFetches(int value) { this.maxSimultaneousFileFetches = value; }

    /**
     * A starting value for the computed average image size. Used to compute the number of fetches to run.
     * to fill the cache.
     */
    @JsonProperty("initialAverageImageSize")
    public long getInitialAverageImageSize() { return initialAverageImageSize; }
    @JsonProperty("initialAverageImageSize")
    public void setInitialAverageImageSize(long value) { this.initialAverageImageSize = value; }


    /**
     * The target value for the cache size. The program will attempt to cache buffer image files until the cache is full.
     * Overshoots are likely, so this should be considered more of a minimum.
     */
    @JsonProperty("cacheSize")
    public long getCacheSize() { return cacheSize; }
    @JsonProperty("cacheSize")
    public void setCacheSize(long value) { this.cacheSize = value; }

    // Image Processing

    /**
     * The number of simultaneous CPU-intensive image analyzers. The number of cores would be an obvious starting point for this #
     */
    @JsonProperty("imageAnalyzerCount")
    public int getImageAnalyzerCount() { return imageAnalyzerCount; }
    @JsonProperty("imageAnalyzerCount")
    public void setImageAnalyzerCount(int value) { this.imageAnalyzerCount = value; }

    /**
     * The maximum number of in process images to track in memory, this limits the size of the linked list of `ImageInProgress'
     * records, not the number of cached files. Because of network timeouts, this list can grow very large before a network read fails,
     * thereby letting the stored results to be written in input order. Network timeouts can be reduced or this number increased (and
     * memory reserved for the list) to prevent the app from blocking this limit.
     */
    @JsonProperty("maxInProgressImages")
    public int getMaxInProgressImages() { return maxInProgressImages; }
    @JsonProperty("maxInProgressImages")
    public void setMaxInProgressImages(int value) { this.maxInProgressImages = value; }

    // Output controls

    /**
     * The maximum number of output file lines that may be written, but not flushed to the output file.
     */
    @JsonProperty("maxLinesBetweenStatusUpdates")
    public int getMaxLinesBetweenStatusUpdates() { return maxLinesBetweenStatusUpdates; }
    @JsonProperty("maxLinesBetweenStatusUpdates")
    public void setMaxLinesBetweenStatusUpdates(int value) { this.maxLinesBetweenStatusUpdates = value; }
}
