package com.pex.exercise.ckornher;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;

/// This file is kept closed and written as atomically as possible (at least it should be in production code)
class ProgressFile {
	private final File file;

	// These two will be in-sync: the latest processed inputLine will correspond to the associated output line number
	long latestProcessedInputLineNumber = -1;
	long associatedOutputLineCount = -1;		// Note that the output file may contain more lines after this

	ProgressFile(File file) {
		this.file = file;
	}

	synchronized boolean exists() {
		return file.exists();
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	synchronized void delete() {
		file.delete();
	}

	synchronized void read() throws IOException {
		CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader(file)));
		String[] strings = csvReader.readNext();
        csvReader.close();

        if( strings.length != 2 ) throw new IOException("File must contain 2 longs");
        latestProcessedInputLineNumber = Long.parseLong(strings[0]);
        associatedOutputLineCount = Long.parseLong(strings[1]);
	}

	synchronized void write(long latestProcessedInputLine, long associatedOutputLine)  throws IOException  {
        CSVWriter writer = new CSVWriter(new FileWriter(file,false));
        latestProcessedInputLineNumber = latestProcessedInputLine;
        associatedOutputLineCount = associatedOutputLine;

        String[] longs = new String[]{Long.toString(latestProcessedInputLineNumber), Long.toString(associatedOutputLineCount)};
        writer.writeNext(longs);
        writer.close();
    }
}
