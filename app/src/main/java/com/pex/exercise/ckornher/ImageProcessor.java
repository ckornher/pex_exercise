package com.pex.exercise.ckornher;

import com.pex.exercise.ckornher.configuration.Config;

//import javax.annotation.Nullable;
import com.sun.istack.internal.Nullable;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("InfiniteLoopStatement")
class ImageProcessor implements Runnable {

	/**
	 * Set this to true ONLY after unit testing the large file support.
	 */
	private final Boolean supportProcessingOfLargeImages = false;

	private final Coordinator coordinator;
	private final int maxPixels;
	private final double maxImageTotalPixels;    // Ignore images with more pixels than this (avoid possible "traps" or the odd mega-image)
	private final int maxColors;
	private final HashMap<Integer, Integer> colorMap;

	ImageProcessor(Coordinator coordinator, Config config) {
		this.coordinator = coordinator;
		this.maxPixels = (int) (config.getMaxImageMemory() / 4);    // up to 2 billion 32 bit pixels. TODO: 24 bit (no alpha) pixels?
		this.maxImageTotalPixels = config.getMaxTotalImageSize();
		this.maxColors = config.getMaxColorsPerImage();

		this.colorMap = new HashMap<>();
	}

	/**
	 * This is run as a daemon thread
	 * This implementation may not handle tiled images efficiently (or at all, possibly)
	 */
	@Override
	public void run() {
		while (true) {
			@Nullable ImageReader reader = null;
			@Nullable ImageInProgress processingRecord = null;
			try {
				try {
					processingRecord = coordinator.nextReadyImage();
				}
				catch (InterruptedException e) {
					continue;    // Blocking queues can do this. Just keep going.
				}

				ImageInputStream stream = ImageIO.createImageInputStream(new BufferedInputStream(new FileInputStream(processingRecord.filePath.toFile())));

				// Get the dimension of the image as quickly as possible (according to the internet...)

				Iterator<ImageReader> readers = ImageIO.getImageReaders(stream);
				if (!readers.hasNext()) {
					throw new IOException("No reader available for supplied image stream.");
				}

				reader = readers.next();

				ImageReadParam imageReaderParam = reader.getDefaultReadParam();
				reader.setInput(stream);

				Dimension imageDimension = new Dimension(reader.getWidth(0), reader.getHeight(0));

				// Split the image in one dimension to make things simple

				long totalPixels = imageDimension.width * imageDimension.height;
				if (totalPixels > maxImageTotalPixels) {
					coordinator.imageProcessingFailed(processingRecord);
					continue;
				}

				List<Rectangle> subRects = null;

				if (totalPixels <= maxPixels) {
					subRects = Collections.singletonList(new Rectangle(0, 0, imageDimension.width, imageDimension.height));
				}
				else if (supportProcessingOfLargeImages) {

					// NOTE: This code has not been tested and is disabled at this time.
					if (imageDimension.width < maxPixels) {
						int chunkHeight = maxPixels / imageDimension.width;
						int fullSizeCount = imageDimension.height / chunkHeight;
						int lastChunkHeight = imageDimension.height % chunkHeight;

						subRects = subdivideImageDimensions(1, imageDimension.width, 0, fullSizeCount, chunkHeight, lastChunkHeight);
					} else if (imageDimension.height < maxPixels) {
						int chunkWidth = maxPixels / imageDimension.height;
						int fullSizeCount = imageDimension.width / chunkWidth;
						int lastChunkWidth = imageDimension.width % chunkWidth;

						subRects = subdivideImageDimensions(fullSizeCount, chunkWidth, lastChunkWidth, 1, imageDimension.height, 0);
					} else {
						// This should not happen since (maxImageTotalPixels < maxPixels * maxPixels) should be true
						coordinator.imageProcessingFailed(processingRecord);
						continue;
					}
				}
				else {
					Logger.getGlobal().log(Level.FINE, "Large image processing not supported for url at line #"
							+ processingRecord.sourceFileLine
							+ " width: " + imageDimension.width + ", height: " + imageDimension.height + "/n");
					coordinator.imageProcessingFailed(processingRecord);
				}

				colorMap.clear(); // paranoia
				assert subRects != null;
				for (Rectangle rect : subRects) {
					imageReaderParam.setSourceRegion(rect);
					BufferedImage image = reader.read(0, imageReaderParam); // Will read only the region specified
					collectColors(image, colorMap, maxColors);
				}

				processingRecord.findTopColors(colorMap);
				coordinator.imageProcessingComplete(processingRecord);
			}
			catch (Exception e) {
				Logger.getGlobal().log(Level.FINEST, "Image Processing Exception:" + e.getLocalizedMessage());

				assert(processingRecord != null);
				coordinator.imageProcessingFailed(processingRecord);
			}
			finally {
				if (reader != null) reader.dispose();
				colorMap.clear();
			}
		}
	}

	/**
	 * NOTE: Grabbing a block of pixels would be faster, perhaps using:
	 * `public int[] getRGB(int startX, int startY, int w, int h,  int[] rgbArray, int offset, int scansize)`
	 * But since the algorithm is not important, this is not done.
	 * Java has a rich image processing API and it would take some experimentation to find most
	 * efficient way to scan the image for colors.
	 * <p>
	 * In theory an image could have a different color for every pixel and this could grow the map to be unacceptably large.
	 * There are a number of techniques that could be used to address this problem, but those are out of scope for this
	 * exercise. This method just throws an exception if a predefined color limit is exceeded. A production app might
	 * flag this image for analysis with a processor with a larger memory footprint or use some other technique.
	 */
	@SuppressWarnings("SpellCheckingInspection")
	private void collectColors(BufferedImage image, HashMap<Integer, Integer> colorMap, int maxColorCount) throws IOException {
		int width = image.getWidth();
		int height = image.getHeight();

		int colorCount = colorMap.size();

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int pixel = image.getRGB(x, y);
				int color = pixel & 0x00ffffff;            // ignore the alpha component

				if (colorMap.containsKey(color)) {
					int count = colorMap.get(color);
					colorMap.put(color, count + 1);
				} else {
					colorCount++;
					if (colorCount > maxColorCount) {
						throw new IOException("Max color count exceeded");
					}
					colorMap.put(color, 1);
				}
			}
		}
	}

	// Utility

	/**
	 * @param nMainWide  The number of standard-with columns
	 * @param mainWidth  The width of the standard-with columns
	 * @param lastWidth  The width of the "remainder" column
	 * @param nMainHigh  The height standard-height rows
	 * @param mainHeight The number of standard-height rows
	 * @param lastHeight The height of the "remainder" row
	 * @return All the chunks for the image
	 */
	private List<Rectangle> subdivideImageDimensions(int nMainWide, int mainWidth, int lastWidth, int nMainHigh, int mainHeight, int lastHeight) {

		List<Rectangle> chunks = new ArrayList<>();

		int y = 0;

		// Get the same-sized rectangular tiles for the "main" area
		for (int i = 0; i < nMainHigh; i++) {
			int x = 0;
			for (int j = 0; j < nMainWide; j++) {
				chunks.add(new Rectangle(x, y, mainWidth, mainHeight));
				x += mainWidth;
			}
			y += mainHeight;
		}

		// Only 1-wide vertical or 1-high horizontal strips required due to size constraints i.e. either rows or columns
		// must be smaller than the max # of pixels that can be processed at one time.
		assert (!((lastHeight > 0) && (lastWidth > 0)));

		if (lastHeight > 0) {
			// Horizontal Strip of mainWidth * lastHeight
			int x = 0;
			y = (nMainHigh - 1) * mainHeight;
			for (int i = 0; i < nMainWide; i++) {
				chunks.add(new Rectangle(x, y, mainWidth, lastHeight));
				x += mainWidth;
			}
		} else if (lastWidth > 0) {
			// Vertical Strip of lastWidth * mainHeight
			int x = (nMainWide - 1) * mainWidth;
			y = 0;
			for (int i = 0; i < nMainHigh; i++) {
				chunks.add(new Rectangle(x, y, lastWidth, mainHeight));
				y += mainHeight;
			}
		}

		return chunks;
	}
}
