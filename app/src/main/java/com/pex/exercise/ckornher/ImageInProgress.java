package com.pex.exercise.ckornher;

//import javax.annotation.Nullable;
import com.sun.istack.internal.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ImageInProgress implements Runnable {

	public final URL url;

	public final long sourceFileLine;

	public final Path filePath;
	private final ImageFileCollector collector;

	// The processing state is maintained ONLY by the Coordinator within thread-safe methods
	// This is done to keep changes in this state in sync with the coordinator. It has the
	// side-benefit of avoiding the creation of locks for these transient objects.
	public ProcessingState processingState = ProcessingState.INITIALIZED;

//	public Date loadStarted;

	public double fileSize;

	public String firstColor = "";

	public String secondColor = "";

	public String thirdColor = "";

	public boolean isFinalImage = false;	// Set after the file has been emptied.
	public final double estimatedFileSize;			/// A rough estimate of how big this file might be

	public ImageInProgress( URL url, long sourceFileLine, ImageFileCollector collector, double estimatedFileSize ) {
		this.url = url;
		this.sourceFileLine = sourceFileLine;
		this.collector = collector;
		this.estimatedFileSize = estimatedFileSize;

		this.filePath = makeFilePath(collector.cacheDirectory);
	}

	// Create a file name from the file line number
	private Path makeFilePath(Path cacheDir) {

		String fileRoot = "L" + sourceFileLine;

		// Preserve the extension if there is one
		String urlString = url.toExternalForm();
		if(urlString.contains(".")) {
			String extension = urlString.substring(urlString.lastIndexOf("."));
			return cacheDir.resolve(fileRoot + extension);
		}
		else {
			return cacheDir.resolve(fileRoot);
		}
	}

	@Override
	/// Use Java NIO to get the file on disk with as (hopefully as close to as few) cycles as possible
	public void run() {
		@Nullable FileChannel fileChannel = null;
		try {
			ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());

			File outputFile = filePath.toFile();
			FileOutputStream fileOutputStream = new FileOutputStream(outputFile,false);

			fileChannel = fileOutputStream.getChannel();
			long transferred = fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
			long currentSize = transferred;
			while( transferred > 0 ){
				transferred = fileChannel.transferFrom(readableByteChannel, currentSize, Long.MAX_VALUE);
				currentSize += transferred;
			}
			fileChannel.close();

			fileSize = currentSize;
			collector.fileHasBeenCached(this);
		}
		catch(IOException e) {
			collector.fileCouldNotBeCached(this);
		}
		finally {
			if( fileChannel != null ) {
				try {
					fileChannel.close();
				}
				catch(IOException e) {
					Logger.getGlobal().log(Level.SEVERE, "Could not close channel for file: " + filePath.toString());
				}
			}
		}
	}

	public void findTopColors(HashMap<Integer, Integer> colorMap) {
		int color1 = 0;
		int color2 = 0;
		int color3 = 0;

		long color1Count = 0;
		long color2Count = 0;
		long color3Count = 0;


		for (Map.Entry<Integer, Integer> entry : colorMap.entrySet()) {
			int count =  entry.getValue();

			if( count > color3Count ) {
				int color = entry.getKey();
				if( count > color2Count ) {
					color3 = color2;
					color3Count = color2Count;
					if (count > color1Count) {
						color2 = color1;
						color2Count = color1Count;

						color1 = color;
						color1Count = count;
					} else {
						color2 = color;
						color2Count = count;
					}
				}
				else {
					color3 = color;
					color3Count = count;
				}
			}
		}

		if( color1Count > 0 ) {
			firstColor = String.format("#%06X", (0xFFFFFF & color1));
		}

		if( color2Count > 0 ) {
			secondColor = String.format("#%06X", (0xFFFFFF & color2));
		}

		if( color3Count > 0 ) {
			thirdColor = String.format("#%06X", (0xFFFFFF & color3));
		}
	}
}
