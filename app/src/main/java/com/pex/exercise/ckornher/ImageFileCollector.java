package com.pex.exercise.ckornher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

class ImageFileCollector {

    /// May be null. A set of URLS that are already in the output file. Gathered at catch-up time.
    private Set<String> ignoreURLS;

    private Double totalFileSizeLoadedForAverage;       // ~10^308 is the max double. We should be safe.
    private Double loadedFileCountForAverage;

    private boolean stopped = false;                    // Stop processing. Ignore anything still in flight.
    private boolean inputExhausted = false;

    private Double estimatedFileSizeInFlight = 0.0;

    private final TopColorApp app;
    private InputFileReader inputReader;
    private final Coordinator coordinator;        // To avoid deadlocks, this class always calls the coordinator, never the opposite
    public Path cacheDirectory;

    // The threads in this pool only exist to make one "zero-copy" from a URL to a file and handle exceptions.
    private ExecutorService downloadExecutor;


    public ImageFileCollector(TopColorApp app,
                              Coordinator coordinator,
                              File inputFile,
                              long startingLineNumber,
                              Set<String> ignoreURLS) throws IOException {
        this.app = app;
        this.coordinator = coordinator;
        this.inputReader = new InputFileReader(inputFile, startingLineNumber,this);

        this.cacheDirectory = app.imageCacheDir;

        this.ignoreURLS = ignoreURLS;

        // Prime the average file size sources
        loadedFileCountForAverage = 10.0;       // Get the real average quickly, but dont be thrown off by the first few files
        totalFileSizeLoadedForAverage = app.config.getInitialAverageImageSize() * loadedFileCountForAverage;

        downloadExecutor = Executors.newFixedThreadPool(app.config.getMaxSimultaneousFileFetches());

        Logger.getGlobal().log(Level.INFO, "Caching image files in: " + this.cacheDirectory );
    }

    public synchronized void start() {
        maintainCache();
    }

    public synchronized void stop() {
        stopped = true;
    }

    /// Called once at app start and then every time a file is cached or an attempt to cache a file fails.
    private synchronized void maintainCache() {
        if(stopped) { return; }


        try {
            // Estimate the empty cache space to be the known empty space minus the estimate of the total size of the files being fetched
            double cacheEmptySpace = coordinator.cacheEmptySpace() - estimatedFileSizeInFlight;

            if( cacheEmptySpace <= 0.0 ) {
                // If cache size is critical, the app could hold-up file fetches that are enqueued but not in-flight.
                // The current algorithm will do for this example.
                Logger.getGlobal().log(Level.FINEST, "No Cache Space. Empty space: "
                        + coordinator.cacheEmptySpace() + ", in est. flight: " + estimatedFileSizeInFlight + "\n");
            }
            else if(!inputExhausted) {
                // Get 1/2 of the number of file that we estimate will fill the cache, but at least one if the cache is not full
                // This factor of .5 will leave some reserve space when files larger than average are cached.
                // It can be added as a tuning parameter if desired.
                int numberOfFilesToFillCache = Math.max((int)(((cacheEmptySpace)/2.0) / averageFileSize()), 1);

                Logger.getGlobal().log(Level.FINEST, "Attempting cache " + numberOfFilesToFillCache
                        + " images for space: " + cacheEmptySpace + "\n");

                for(int i = 0; i<numberOfFilesToFillCache && !inputExhausted; i++) {
                    if(!coordinator.canProcessMore()) {
                        // This should be relatively infrequent, so this strategy should be efficient enough.
                        // Avoid deadlocks by asking the coordinator to run this task in a new thread when it can process more.
                        // Required when all files are cached or the cache is empty, and the coordinator cannot process any more.
                        Runnable continueProcessingTask = () -> {
                            this.maintainCache();
                            Logger.getGlobal().log(Level.FINER, "UNBLOCKED ------- Coordinator has freed up space for more images.\n");
                        };
                        Logger.getGlobal().log(Level.FINER, "BLOCKED ------- Waiting for coordinator to free-up space to process more files.\n");
                        coordinator.runWhenCanProcessMore( continueProcessingTask );
                        return;
                    }

                    // Get the next file to read, with a (very rough) estimate of its size
                    double estimatedFileSize = averageFileSize();
                    ImageInProgress nextImage = inputReader.nextImageToProcess(estimatedFileSize);

                    if( nextImage == null) {
                        Logger.getGlobal().log(Level.FINE, "Input has been exhausted\n");
                        inputExhausted = true;
                        coordinator.inputHasBeenExhausted();
                    }
                    else {
                        estimatedFileSizeInFlight += estimatedFileSize;
                        String urlString = nextImage.url.toString();

                        if((ignoreURLS != null) && (ignoreURLS.contains(urlString))) {
                            // This will slow the cache fill on restarts. It can be improved if restarts are common.
                            Logger.getGlobal().log(Level.FINER, "Ignoring URL already written: " + urlString + "\n");
                            coordinator.fileNotCached(nextImage);
                        }
                        else {
                            coordinator.nextImageStarting(nextImage);
                            downloadExecutor.execute(nextImage);
                        }
                    }
                }
            }
        } catch(IOException e) {
            app.stopWithException(e, true);
        }
    }

    // Must be called from synchronized method
    private double averageFileSize() {
       return totalFileSizeLoadedForAverage / loadedFileCountForAverage;
    }

    public synchronized void fileHasBeenCached(ImageInProgress imageRecord) {
        if(stopped) { return; }

        estimatedFileSizeInFlight -= imageRecord.estimatedFileSize;
        totalFileSizeLoadedForAverage += imageRecord.fileSize;
        loadedFileCountForAverage += 1.0;

        coordinator.fileHasBeenCached(imageRecord);
        maintainCache();
    }

    public synchronized void fileCouldNotBeCached(ImageInProgress imageRecord) {
        if(stopped) { return; }

        estimatedFileSizeInFlight -= imageRecord.estimatedFileSize;
        coordinator.fileNotCached(imageRecord);
        maintainCache();
    }
}
