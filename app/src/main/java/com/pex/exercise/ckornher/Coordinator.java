package com.pex.exercise.ckornher;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.application.Platform.exit;


class Coordinator {

	/// Images that have been read from the input file and are in-flight
	/// This list is in input-file-line-order
	private final LinkedList<ImageInProgress> inProgress = new LinkedList<>();

	private final BlockingQueue<ImageInProgress> readyImages = new LinkedBlockingQueue<>();

	private final TopColorApp app;
	private boolean stopped = false;

	private double cacheSize;
	private final double maxCacheSize;
	private final int maxInProgress;

	/**
	 * There is only one originator of this runnable: the file collector, so we only need one instance of this runnable
	 * at this time.
	 */
	private Runnable canProcessMoreCallback;

	/// Input to the Processing Recorder
	public final BlockingQueue<List<ImageInProgress>> processedImageLists = new LinkedBlockingQueue<>();

	public Coordinator(TopColorApp app) {
		this.app = app;
		maxCacheSize = app.config.getCacheSize();
		maxInProgress = app.config.getMaxInProgressImages();
	}

	public synchronized void stop() {
		stopped = true;
	}

	public synchronized void inputHasBeenExhausted() {
		if(inProgress.size() == 0) {
			app.completed();
		} else {
			/// This is guaranteed to be "recorded" last, as all image records are "recorded" in input order
			inProgress.getLast().isFinalImage = true;
		}
	}

	// Thread-safe image pipeline methods.

	public synchronized void nextImageStarting(ImageInProgress imageRecord) {
		if(stopped) { return; }

		imageRecord.processingState = ProcessingState.FETCHING;

		inProgress.add(imageRecord);
	}

	// Callbacks from file caching

	/// Get the next image ready for reading. Its file has been downloaded to the cache
	public synchronized void fileHasBeenCached(ImageInProgress imageRecord) {
		Logger.getGlobal().log(Level.FINEST, "File cached: " + imageRecord.filePath );

		imageRecord.processingState = ProcessingState.FETCHED;
		cacheSize += imageRecord.fileSize;

		try {
			readyImages.put(imageRecord);
		}
		catch(InterruptedException e) {
			// This should never wait or be interrupted. Assume that is an out of memory or deadlock condition
			app.stopWithException(e, false);
		}
	}

	// The file could not be fetched from the URL. This terminates the processing of this image.
	public synchronized void fileNotCached(ImageInProgress imageRecord) {
		Logger.getGlobal().log(Level.FINE, "File NOT cached: " + imageRecord.filePath );

		imageRecord.processingState = ProcessingState.NOT_FETCHED;
		checkInProgress();
	}

	/// NOT Synchronized
	/// Serve an image file to the color reader(s)
	public ImageInProgress nextReadyImage() throws InterruptedException {
		// The following call blocks
		ImageInProgress imageRecord = readyImages.take();

		synchronized(this) {
			imageRecord.processingState = ProcessingState.PROCESSING;

		}
		return imageRecord;
	}

	// Callbacks from image processing

	/// All file processing is complete. Output and status files need to be updated.
	public synchronized void imageProcessingComplete(ImageInProgress imageRecord) {
		if(stopped) { return; }

		Logger.getGlobal().log(Level.FINEST, "imageProcessingComplete: " + imageRecord.sourceFileLine + "\n");

		imageRecord.processingState = ProcessingState.COMPLETE;
		checkInProgress();
	}

	/// All file processing is complete. Output and status files need to be updated.
	public synchronized void imageProcessingFailed(ImageInProgress imageRecord) {
		if(stopped) { return; }

		Logger.getGlobal().log(Level.FINEST, "imageProcessingComplete: " + imageRecord.sourceFileLine + "\n");

		imageRecord.processingState = ProcessingState.FAILED_TO_PROCESS;
		checkInProgress();
	}

	// Thread safe stats and counts methods

	public synchronized double cacheEmptySpace() {
		return Math.max(maxCacheSize - cacheSize, 0.0);
	}

	// Output Control Methods

	/**
	 *
	 */
	private synchronized void checkInProgress() {
		List<ImageInProgress> completed = new ArrayList<>();

		// Collect all the completed records at the beginning on the list, preserving the input file order.
		ListIterator<ImageInProgress> itr = inProgress.listIterator();
		while (itr.hasNext()) {
			ImageInProgress next = itr.next();
			if (next.processingState.isEndState()) {
				completed.add(next);

				// Subtract the file from the calculated cache size now. It will be deleted very shortly
				cacheSize -= next.fileSize;
			} else {
				break;
			}
		}

		Logger.getGlobal().log(Level.FINEST, ": " + "Coordinator has " + inProgress.size() + " in progress with " + completed.size() + " ready to write\n");

		if(completed.size() > 0) {
			try {
				// Hand off the list of completed images(successes and failures) to the output writer
				processedImageLists.put(completed);
			} catch (InterruptedException e){
				exit(); // This cannot happen in normal operation
			}

			// Remove the completed images (from the head of the list) and notify file collector, if it is waiting
			inProgress.removeAll(completed);
			notifyCanProcessMoreIfRequired();
		}
	}

	//
	synchronized boolean canProcessMore() {
		return inProgress.size() < maxInProgress;
	}

	/**
	 * Set-up the fully asynchronous callback to potentially turn the file collector back on.
	 * This is only called by the File Collector at this time, so we only need one instance of this callback.
	 * More instances would take-up more memory and possibly might flood the cache
	 */
	synchronized void runWhenCanProcessMore(Runnable canProcessMoreCallback) {
		this.canProcessMoreCallback = canProcessMoreCallback;

		// Avoid a race condition between calls to canProcessMore() and this method
		// Avoids requiring the creation of a runnable with every test.
		notifyCanProcessMoreIfRequired();
	}

	private synchronized void notifyCanProcessMoreIfRequired() {
		if( canProcessMore() && canProcessMoreCallback != null) {
			new Thread(canProcessMoreCallback).start();		// Poor-man's asynchronous notification.
			canProcessMoreCallback = null;
		}
	}
}
