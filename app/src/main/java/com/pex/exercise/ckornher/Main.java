package com.pex.exercise.ckornher;

class Main {
    public static void main(String[] args) {
        // TODO: parse command line configuration

        if(args.length == 2) {
            TopColorApp app = new TopColorApp( args[0], args[1], false);
            app.start();
        }
        else if(args.length == 3 && (args[2].toLowerCase().equals("reset"))) {
            TopColorApp app = new TopColorApp( args[0], args[1], true);
            app.start();
        }
        else {
            System.out.println("\nTopColorApp takes 2 arguments or three argument:");
            System.out.println("    1) the path to the input file");
            System.out.println("    2) the path to the JSON config file");
            System.out.println("    3) (optional) the string 'reset'" );
            System.out.println("\nNOTE: A production app would have more comprehensive command line support and help,");
            System.out.println("      but that is not the focus of this exercise (I hope).\n");
        }
    }
}
